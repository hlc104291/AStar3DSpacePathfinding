﻿using UnityEngine;
using System.Collections;
using System;

namespace Hont.AStar
{
    public interface IHontAStarUnityMaskCustomEnum
    {
        Enum MaskEnum { get; }
    }
}
